<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Blogpost;
use App\Entity\Categorie;
use App\Entity\Peinture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Faker\Factory;
use DateTimeImmutable;
use DateTime;

/**
 * @codeCoverageIgnore
 */
class AppFixtures extends Fixture
{
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }


    public function load(ObjectManager $manager): void
    {
        // Utilisation de Faker
        require_once 'vendor/autoload.php';
        $faker = Factory::create('fr_FR');
        // Création d'un utilisateur
        $user = new User();
        $user->setEmail('user@test.com')
             ->setPrenom($faker->firstName())
             ->setNom($faker->firstName())
             ->setTelephone($faker->phoneNumber())
             ->setAPropos($faker->text())
             ->setInstagram('instagram')
             ->setRoles(['ROLE_PEINTRE']);

        $password = $this->passwordHasher->hashPassword($user, 'password');
        $user->setPassword($password);

        $manager->persist($user);

        // Création de 10 blogpost
        for ($i = 0; $i < 10; $i++) {
            //Création DateTimeImmutable
            $date = $faker->dateTimeBetween('-6 month', 'now');
            $immutable = DateTimeImmutable::createFromMutable($date);

            $blogpost = new Blogpost();

            $blogpost->setTitre($faker->words(3, true))
                     ->setCreatedAt($immutable)
                     ->setContenu($faker->text(350))
                     ->setSlug($faker->slug(3))
                     ->setUser($user);

            $manager->persist($blogpost);
        }

        // Création d'un Blogpost pour les tests
        $blogpost = new Blogpost();

        // Création DateTimeImmutable
        $date = $faker->dateTimeBetween('-6 month', 'now');
        $immutable = DateTimeImmutable::createFromMutable($date);

        $blogpost->setTitre('blogpost test')
                 ->setCreatedAt($immutable)
                 ->setContenu($faker->text(350))
                 ->setSlug('blogpost-test')
                 ->setUser($user);

        $manager->persist($blogpost);

        // Création de 5 Catégories
        for ($k = 0; $k < 5; $k++) {
            $categorie = new Categorie();

            $categorie->setNom($faker->word())
                      ->setDescription($faker->words(10, true))
                      ->setSlug($faker->slug());

            $manager->persist($categorie);
            // Création de 2 Peintures/catégorie
            for ($j = 0; $j < 2; $j++) {
                //Création DateTimeImmutable
                $date = $faker->dateTimeBetween('-6 month', 'now');
                $immutable = DateTimeImmutable::createFromMutable($date);

                $peinture = new Peinture();

                $peinture->setNom($faker->words(3, true))
                         ->setLargeur($faker->randomFloat(2, 20, 60))
                         ->setHauteur($faker->randomFloat(2, 20, 60))
                         ->setEnVente($faker->randomElement([true, false]))
                         ->setDateRealisation($faker->dateTimeBetween('-6 month', 'now'))
                         ->setCreatedAt($immutable)
                         ->setDescription($faker->text())
                         ->setPortfolio($faker->randomElement([true, false]))
                         ->setSlug($faker->slug())
                         ->setFile('monet5.jpg')
                         ->addCategorie($categorie)
                         ->setPrix($faker->randomFloat(2, 100, 9999))
                         ->setUser($user);

                $manager->persist($peinture);
            }
        }

        // Catégorie de test
        $categorie = new Categorie();

        $categorie->setNom('categorie test')
                  ->setDescription($faker->words(10, true))
                  ->setSlug('categorie-test');

        $manager->persist($categorie);

        //PEINTURE pour les TESTS
        $peinture = new Peinture();

        //Création DateTimeImmutable
        $date = $faker->dateTimeBetween('-6 month', 'now');
        $immutable = DateTimeImmutable::createFromMutable($date);

        $peinture->setNom('peinture test')
                 ->setLargeur($faker->randomFloat(2, 20, 60))
                 ->setHauteur($faker->randomFloat(2, 20, 60))
                 ->setEnVente($faker->randomElement([true, false]))
                 ->setDateRealisation($faker->dateTimeBetween('-6 month', 'now'))
                 ->setCreatedAt($immutable)
                 ->setDescription($faker->text())
                 ->setPortfolio($faker->randomElement([true, false]))
                 ->setSlug('peinture-test')
                 ->setFile('monet5.jpg')
                 ->addCategorie($categorie)
                 ->setPrix($faker->randomFloat(2, 100, 9999))
                 ->setUser($user);

        $manager->persist($peinture);

        $manager->flush();
    }
}
